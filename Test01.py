import requests
import json
from requests.models import Response


def test_create():
    response = requests.post('http://localhost:55000/bear', json={'bear_type':'BLACK','bear_name':'mikhail02','bear_age':'17.5'})
    try:
        assert response.status_code == 200
    except:
        print("ERROR test_create: Should be 200")

    #print(response.json())
    a = response.json()

    return(a)


def test_edit(b):
    response = requests.put(f'http://localhost:55000/bear/{b}', json={'bear_type':'GUMMY','bear_name':'Rename','bear_age':'50'})
    try:
        assert response.status_code == 200
    except:
        print("ERROR test_edit: Should be 200")

    #print(response.content)


def test_get_id(b):
    response = requests.get(f'http://localhost:55000/bear/{b}')
    try:
        assert response.status_code == 200
    except:
        print("ERROR test_get_id: Should be 200")

    c = response.json()

    try:
        assert c["bear_type"] == 'GUMMY'
    except:
        print("ERROR test_get_id: Should be GUMMY")
    try:
        assert c["bear_name"] == 'Rename' 
    except:
        print("ERROR test_get_id: Should be 50")
    try:
        assert c["bear_age"] == '50'
    except:
        print("ERROR test_get_id: Should be Rename")
    
    #print(response.content) 


def test_delete_id(b):
    response = requests.delete(f'http://localhost:55000/bear/{b}')
    try:
        assert response.status_code == 200
    except:
        print("ERROR test_delete_id: Should be 200")

    #print(response.status_code)


def test_get_id_after_delete(b):
    response = requests.get(f'http://localhost:55000/bear/{b}')
    try:
        assert response.status_code == 200
    except:
        print("ERROR test_get_id_after_delete: Should be 200")

    i=response.text

    try:
        assert i == 'EMPTY',"Should be EMPTY"
    except:
        print("ERROR test_get_id_after_delete: Should be EMPTY")

    #print(i)


def test_create_2():
    response = requests.post('http://localhost:55000/bear', json={'bear_type':'BLACK','bear_name':'mikhail02','bear_age':'17.5'})
    try:
        assert response.status_code == 200
    except:
        print("ERROR test_create_2: Should be 200")

    #print(response.status_code)


def test_create_3():
    response = requests.post('http://localhost:55000/bear', json={'bear_type':'BLACK','bear_name':'mikhail02','bear_age':'17.5'})
    try:
        assert response.status_code == 200
    except:
        print("ERROR test_create_3: Should be 200")

    #print(response.status_code)


def test_get_all():
    response = requests.get('http://localhost:55000/bear')
    try:
        assert response.status_code == 200
    except:
        print("ERROR test_get_all: Should be 200")

    d=len(response.json())
    #print(d)

    try:
        assert d == 2, "Should be 2"
    except:
        print("ERROR test_get_all: Should be 2")


def test_delete_all():
    response = requests.delete('http://localhost:55000/bear')
    try:
        assert response.status_code == 200
    except:
        print("ERROR test_delete_all: Should be 200")

    #print(response.status_code)


def test_get_all_after_delete_all():
    response = requests.get('http://localhost:55000/bear')
    try:
        assert response.status_code == 200
    except:
        print("ERROR test_get_all_after_delete_all: Should be 200")

    t=len(response.json())
    #print(t)

    try:
        assert t == 0
    except:
        print("ERROR test_get_all_after_delete_all: Should be 0")


if __name__ == "__main__":
    b = test_create()
    test_edit(b)
    test_get_id(b)
    test_delete_id(b)
    test_get_id_after_delete(b)
    test_create_2()
    test_create_3()
    test_get_all()
    test_delete_all()
    test_get_all_after_delete_all()
    print("Done")

    
